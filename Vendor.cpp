#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <set>
#include <memory>
#include <numeric>
#include <algorithm>
#include <iterator>



#include "Vendor.h"

using namespace std;

  // Overload insertion operator << for Fruit
  // This overloaded function must be a friend of Fruit 
  // since it will access the private member data of Fruit 

  std::ostream & operator<< (std::ostream &out, const Fruit *r) {

    string id = to_string (r->mid) ;
    string p = "$" + to_string (r->munitPrice) + "/oz" ;
    string s = to_string(r->mweight) + "oz" ;
    out << setw(10) << id << setw(15) << r->mname << setw(15) << p 
    << setw (15) << s ;
    s = "$" + to_string(r->getPrice()) ;
    out << setw (15) << s ;
    s = r->getColor() ;
    out << setw (15) << s ;
    return out;
  }

  std::ostream & operator<< (std::ostream &out, const unique_ptr<Fruit> & r) {
    out << r.get();    
    return out;
  }

  // Overload insertion operator << for Fruit
  // This overloaded function must be a friend of Fruit 
  // since it will access the private member data of Fruit 

  std::ostream & operator<< (std::ostream &out, const Vendor& r) {
    out << "#" << setw(10) << "ID" << setw(15) << "Name" << setw(15) \
      << "Unit_Price" << setw(15) \
      << "Weight(oz)" <<  setw(15) << "Total_Price" << setw(15) << "Color" << endl;
    string s (10, '-');
    out << "#" ;
    for (size_t i=0; i < 6; ++i ) {
      out << s << setw(15) ;
    }
    out << endl;
    for ( auto& f : r.mfruit ) {
      out << f << endl; 
    }
    out << "#Total price: $" << r.getTotalPrice () << endl;;
    return out;
  }



  void Vendor::collectFruit ( size_t tot ) {
    vector<string> names { "apple", "orange" };
    vector<string> colors { "red", "yellow" };

    for (size_t i=0; i < tot; ++i ) {
      size_t c = rand()%colors.size(); // c is random index to array collors
      size_t j = rand()%names.size(); // j is the random index to array names 
      int p = rand()%3+1;             // p is a random unit price between $1 and $3
      int w = rand()%5+1;             // w is a random weight between 1 and 5 (oz)
      Fruit* q ;

      q = new Fruit( names[j], p,w, mfruit.size(), colors[c])  ; 
      mfruit.emplace_back ( q );
    }
    cout << mname << " collects " << tot << " new fruit" << endl;

  }

  Vendor::Vendor ( const string& name ) : mname(name) 
  {
  }

  int Vendor::getTotalPrice ( )  const {
    return std::accumulate( mfruit.begin(),  mfruit.end(), 0, 
                                    [](int a, const unique_ptr<Fruit>& b ) {
                                        return a + b->getPrice() ;
                                    });
  }

bool comparePrice(const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) {
    return x->getPrice() < y->getPrice();
}
bool compareWeight(const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) {
    return x->getWeight() < y->getWeight();
}
bool compareName(const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) {
    return x->getName() < y->getName();
}
bool compareColor(const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) {
    return x->getColor() < y->getColor();
}

// This is the function object for sorting fruits
bool FruitCompare::operator() (const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) const 
{
  switch ( mmode ) {
    case CompareMode::PRICE:
      return x->getPrice() < y->getPrice();
      break;
    case CompareMode::WEIGHT:
      return x->getWeight() < y->getWeight();
      break;
    case CompareMode::NAME:
      return x->getName() < y->getName();
      break;
    case CompareMode::COLOR:
      return x->getColor() < y->getColor();
      break;
    case CompareMode::DEFAULT:
      return *x < *y; // 
      break;
    default:
      return x->getId() > y->getId(); // 
      break;
  };
}

void Vendor::sortDB ( CompareMode mode, bool withFunctor )  {
  if ( withFunctor ) {
    sortWithFunctor ( mode)  ;
  } else {
    sortWithoutFunctor ( mode)  ;
  }
}

void Vendor::sortWithFunctor ( CompareMode mode)  {
  // a functor that compares fruits based on the given comparison mode
  FruitCompare myCompare (mode) ; 

  string name = myCompare.modeName();

  cout << mname << " sorts fruits by " << name << " using functor." << endl;
  std::sort ( mfruit.begin(), mfruit.end(), myCompare );
}


void Vendor::sortWithoutFunctor( CompareMode m) 
{
  // TODO: implement sortDB using comparing funcitons instead of Functor.
}


void Vendor::saveDB( const string& fn) const
{
  ofstream fout (fn);
  cout << "saveDB: file " << fn << endl;
  
  // TODO: save DB in a plain text file
  fout.close();
}

// TODO: report the histogram of the fruit names  
void Vendor::reportDB( const string& fn) const {
  ofstream fout (fn);
  cout << "reportDB: file " << fn << endl;
  
  fout.close();
}

void Vendor::saveBinaryDB( const string& fn) const {
  ofstream fout (fn);
  cout << "saveBinaryDB: file " << fn << endl;
  
  // TODO: to be finished 

  fout.close();
}

int extractInteger ( const string& s ) {
    string temp ;
    for ( auto& ch: s ) {
      if ( isdigit ( ch ) ) {
        temp += ch;
      }
    }
    return stoi(temp) ;
}
void Vendor::readDB( const string& fn) {
  ifstream fin (fn);
  string line ;
  cout << "readDB: file " << fn << endl;

  size_t cnt = 0  ;
  size_t duplicate = 0  ;
  set<size_t>myset ;
  for (const auto& p: mfruit ) {
    myset.insert ( p->getId() ) ;
  }
  while  (getline ( fin, line ) ) {
    istringstream iss( line );
    vector<string> tokens {istream_iterator<string>{iss}, istream_iterator<string>{}};
    size_t n = tokens.size();
    if (n < 1 )  continue;
    if ( tokens[0][0] == '#' ) continue;
    // cout << line << endl;

    size_t id = stoi ( tokens[0] );
    if ( myset.find(id) == myset.end() ) {
      const string& species = tokens[1]; 
      int p = extractInteger ( tokens[2] );
      int w = extractInteger ( tokens[3] );
      string color = tokens[5];
    
      Fruit* q = new Fruit( species, p,w, id, color );
      mfruit.emplace_back ( q );
      cnt++;
      myset.insert ( id );
    } else {
      duplicate++;
    }
  }
  if ( cnt > 0 ) {
    cout << "ReadDB: read " << cnt << " entries." << endl; 
  }
  if ( duplicate > 0 ) {
    cout << "ReadDB: find " << duplicate << " duplicate entries." << endl; 
  }
  fin.close();
}


#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>

#include "Fruit.h"

using namespace std;

  Fruit::Fruit ( const string& name, int p, int w, size_t id, const std::string& c) : 
    mname(name), munitPrice (p), mweight (w), mid(id), mColor(c)
  {
  }

  Fruit::Fruit ( const string& name, int p, int w, size_t id) : 
    mname(name), munitPrice (p), mweight (w), mid(id)
  {
  }

  const std::string&  Fruit::getColor() const  {
    return mColor;
  }

  int Fruit::getPrice() const  {
    return munitPrice * mweight ;
  }
  int Fruit::getWeight() const  {
    return mweight ;
  }
  const string& Fruit::getName() const  {
    return mname ;
  }

  // Default comparison criteria
  // overload operator <, then  we can compare Fruits (i.e., we can compare orange and apple)
  bool Fruit::operator< (const Fruit &o)  const {

    return mid < o.mid  ;
  }


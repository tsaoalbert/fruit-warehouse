John collects 10 fruit
        ID           Name     Unit_Price     Weight(oz)    Total_Price
----------     ----------     ----------     ----------     ----------
         0         orange          $2/oz            4oz             $8
         1          apple          $2/oz            3oz             $6
         2          apple          $3/oz            4oz            $12
         3         orange          $1/oz            1oz             $1
         4          apple          $3/oz            3oz             $9
         5         orange          $2/oz            5oz            $10
         6          apple          $2/oz            4oz             $8
         7         orange          $1/oz            3oz             $3
         8          apple          $1/oz            5oz             $5
         9          apple          $3/oz            1oz             $3

John sorts fruits by PRICE.
        ID           Name     Unit_Price     Weight(oz)    Total_Price
----------     ----------     ----------     ----------     ----------
         3         orange          $1/oz            1oz             $1
         9          apple          $3/oz            1oz             $3
         7         orange          $1/oz            3oz             $3
         8          apple          $1/oz            5oz             $5
         1          apple          $2/oz            3oz             $6
         0         orange          $2/oz            4oz             $8
         6          apple          $2/oz            4oz             $8
         4          apple          $3/oz            3oz             $9
         5         orange          $2/oz            5oz            $10
         2          apple          $3/oz            4oz            $12

John sorts fruits by WEIGHT.
        ID           Name     Unit_Price     Weight(oz)    Total_Price
----------     ----------     ----------     ----------     ----------
         3         orange          $1/oz            1oz             $1
         9          apple          $3/oz            1oz             $3
         7         orange          $1/oz            3oz             $3
         4          apple          $3/oz            3oz             $9
         1          apple          $2/oz            3oz             $6
         0         orange          $2/oz            4oz             $8
         6          apple          $2/oz            4oz             $8
         2          apple          $3/oz            4oz            $12
         8          apple          $1/oz            5oz             $5
         5         orange          $2/oz            5oz            $10

John sorts fruits by DEFAULT.
        ID           Name     Unit_Price     Weight(oz)    Total_Price
----------     ----------     ----------     ----------     ----------
         1          apple          $2/oz            3oz             $6
         2          apple          $3/oz            4oz            $12
         4          apple          $3/oz            3oz             $9
         6          apple          $2/oz            4oz             $8
         8          apple          $1/oz            5oz             $5
         9          apple          $3/oz            1oz             $3
         0         orange          $2/oz            4oz             $8
         3         orange          $1/oz            1oz             $1
         5         orange          $2/oz            5oz            $10
         7         orange          $1/oz            3oz             $3


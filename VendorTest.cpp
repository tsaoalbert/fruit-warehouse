#include <iostream>
#include <iomanip>
#include <vector>
#include <set>
#include <memory>

#include "Vendor.h"

using namespace std;



int main () 
{
  Vendor vendor("John" );
  // vendor.collectFruit ( 10 );
  // vendor.saveDB ( "db1.txt" );

  vendor.readDB ( "db1.txt" );
  vendor.readDB ( "db2.txt" );

  vendor.reportDB ( "report.txt" );

  // TODO: new way to sort fruits
  vendor.sortWithoutFunctor (CompareMode::NAME ); 

  vendor.saveDB ( "db3.txt" );
  vendor.saveBinaryDB ( "db3.dat" );
}

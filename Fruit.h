#pragma once

#include <iostream>
#include <string>
#include <memory>
using namespace std;

class Fruit {
  friend std::ostream &operator<< (std::ostream &out, const Fruit *r) ;

  std::string mname;
  
  int  munitPrice;
  int  mweight;
  size_t mid;

  std::string mColor;

public:
  Fruit () = default;
  Fruit ( const string& , int , int , size_t , const std::string& ) ; 
  Fruit (const string& name, int p, int w, size_t id) ;

  // overload operator <, then  we can compare Fruits (i.e., we can compare orange and apple)
  bool operator< (const Fruit &o) const ;
  int getWeight() const  ;
  const string& getName() const  ;
  int getPrice() const  ;
  int getId() const  { return mid; } ;

  const string& getColor() const  ;

};

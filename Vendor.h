#pragma once

#include <iostream>
#include <string>
#include <memory>

#include "Fruit.h" 

using namespace std;



enum class CompareMode {
  NAME,
  PRICE,
  WEIGHT,
  COLOR,
  DEFAULT
};

struct FruitCompare {
  CompareMode mmode ;

  FruitCompare ( ) = default ;
  FruitCompare (CompareMode m ) { setCompareMode (m); }

  static string modeName ( CompareMode m) {
    vector<string> names { "NAME", "PRICE", "WEIGHT", "COLOR","DEFAULT" };
    return names[ static_cast<size_t> ( m ) ] ; // xxx
  }
  string modeName ( ) const {
    return modeName ( mmode );
  }

  bool operator() (const Fruit* x, const Fruit* y) const ;
  bool operator() (const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) const ;

  void setCompareMode ( CompareMode m ) {
    mmode =  m;
  }
};
static CompareMode mmode = CompareMode::DEFAULT;

class Vendor {
private:
  friend std::ostream & operator<< (std::ostream &out, const Vendor& r) ;

  void sortWithFunctor( CompareMode m) ;

  std::string mname;
  vector < unique_ptr<Fruit> > mfruit;

public:
  // TODO: new way to sort the fruit
  void sortWithoutFunctor( CompareMode m) ;
  void sortDB ( CompareMode mode, bool withFunctor = true )  ;
  
  Vendor (const string& ) ;
  void collectFruit ( size_t ) ;
  int getTotalPrice ( )  const ;

public:
  // TODO:
  void reportDB ( const string& )  const ;

  void readDB ( const string& )  ;

  // TODO: read DB from a binary file
  void readBinaryDB ( const string& )  ;

  // TODO: save DB in a plain text file
  void saveDB ( const string& )  const ;

  // TODO: save DB in a binary file
  void saveBinaryDB ( const string& )  const ;

};
